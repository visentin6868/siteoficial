<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Grupo Visentin Empresarial</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <meta http-equiv="content-language" content="pt-br, en-US, fr" />

  <!-- Favicons -->
  <link href="img/favicon.ico" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Tema nome: Grupo Visentin 
    Tema URL: https://grupovisentin.com.br
    Author: aparecido visentin
    
  ======================================================= -->
</head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a href="#intro" class="scrollto">GV<u>E</u></a></h1>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">Home</a></li>
          <li><a href="#about">Sobre Nós</a></li>
          <li><a href="#services">Serviços</a></li>
          <li><a href="#team">Time</a></li>
          <li><a href="#contact">Contato</a></li>
          <li><a href="https://helpdesk.grupovisentin.com.br"target="blank_">Suporte</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active">
            <div class="carousel-background"><img src="img/banner/web_dev.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Hospedagem e Desenvolvimento Web-Sites</h2>
                <p>Responsividade e planejamento do seu Site, rankeamento nas primeiras páginas do Google com valores acessiveis, suporte 24/7.</p>
                <a href="#contact" class="btn-get-started scrollto">Entre em Contato.</a>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="img/banner/segurança.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Segurança e Mobilidade.</h2>
                <p>Com segurança não se brinca, porisso o Grupo Visentin possui as melhores parcerias do mercado com os melhores antivirus do mercado para sua Empresa.</p>
                <a href="#services" class="btn-get-started scrollto">Nossos Produtos</a>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="img/banner/markting.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Criação de Logos</h2>
                <p>Sua Empresa precisa de uma MARCA? Uma Idendtidade Visual?<br> Nos do Grupo Visentin temos as melhores criações de imagens e logos nas melhores atualidades do mercado. .</p>
                <a href="#services" class="btn-get-started scrollto">Saiba Mais!</a>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="img/banner/windows.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Licenças Windows / Office</h2>
                <p>Sua Empresa precisa de Licenças Windows 7, 8 ou 10. Nós do Grupo Visentin em parceria com a Microsoft possui este recurso em licenciamentos.</p>
                <a href="#services" class="btn-get-started scrollto">Ver Mais</a>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src="img/banner/cloud.jpg" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>Compartilhamento em Nuvem?</h2>
                <p>Solução em Cloud personalizado para empresas, Monitoramento em compartilhamento de arquivos, Planos Acessiveis.</p>
                <a href="#contact" class="btn-get-started scrollto">Entre em contato!</a>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 box">
            <i class="ion-ios-bookmarks-outline"></i>
            <h4 class="title"><a href="#contact" class="btn-get-started scrollto">Satisfação do Cliente</a></h4>
            <p class="description">Para nós não basta resolver os problemas. Mas sim causar a melhor impressão para cada cliente. Satisfação do cliente é a nossa maior preocupação.</p>
          </div>

          <div class="col-lg-4 box box-bg">
            <i class="ion-ios-stopwatch-outline"></i>
            <h4 class="title"><a href="#contact" class="btn-get-started scrollto">Suporte 24/7</a></h4>
            <p class="description">Suporte personalizado e com total atenção ao cliente.<br>Servidores com total disponibilidade do mercado com 99,9% UPTime. </p>
          </div>

          <div class="col-lg-4 box">
            <i class="ion-ios-pulse"></i>
            <h4 class="title"><a href="#contact" class="btn-get-started scrollto">Análise de Mercado</a></h4>
            <p class="description">A análise de mercado é um dos principais elementos do plano de negócio e, sem ela, será impossível conhecer as necessidades do seu público-alvo, o perfil da concorrência e os melhores fornecedores.</p>
          </div>

        </div>
      </div>
    </section><!-- #featured-services -->

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>Sobre Nós</h3>
          <p> Grupo Visentin Epresarial esta no mercado desde 2017, fundado pelo CEO <a href="#team" class="btn-get-started scrollto">Aparecido visentin </a>e a equipe sempre desenvolvendo inovação e tecnologia no mercado para melhorias de pequenas e grandes empresas. </p>
        </header>

        <div class="row about-cols">

          <div class="col-md-4 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="img/about-mission.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              </div>
              <h2 class="title"><a href="#contact" class="btn-get-started scrollto">Missão</a></h2>
              <p>
              Planejar e executar soluções em TI e inovação para o crescimento e gestão de empresas de vários seguimentos.
              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="about-col">
              <div class="img">
                <img src="img/about-plan.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-list-outline"></i></div>
              </div>
              <h2 class="title"><a href="#contact" class="btn-get-started scrollto">Valores</a></h2>
              <p>
              Manter o comprometimento e integridade, valorizando os colaboradores e parceiros, visando evolução constante, inovação, superação, transparência e resultados, promovendo assim uma alta sustentabilidade e bom relacionamento com nossos clientes.
              </p>
              

            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="img/about-vision.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-eye-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Visão</a></h2>
              <p>
              Ser reconhecida como uma empresa de recursos e soluções em tecnologia web e móvel para o desenvolvimento e ampliação de nossos clientes.
              </p>
            </div>
          </div>

        </div>

      </xdiv>
    </section>
    <!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">

        <header class="section-header wow fadeInUp">
          <h3>Serviços</h3>
          <p>Com um alto conhecimento em serviços prestados, temos a satisfação de oferecer nossos serviços com satisfação e disponibilidade 24/7 para nossos clientes, veja abaixo nosso portifolio de serviços.</p>
        </header>

        <div class="row">

          <div class="col-lg-4 col-md-8 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
            <h4 class="title"><a href="">Desenvolvimento de Web Sites</a></h4>
            <p class="description">Desenvolvemos seu site com a cara da sua empresa, sites responsivos que se adaptam a qualquer tela de dispositivos. Valores acessiveis e planos que cabem no seu bolso! </p>
          </div>
          <div class="col-lg-4 col-md-8 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
            <h4 class="title"><a href="">Revenda de Hospedagem</a></h4>
            <p class="description">É Desenvovedor? e precisa de uma hospedagem! Fale conosco planos especiais para revenda %99,9 Uptime dos nossos servidores, Segurança contra DDOS, Certificado SSL Free em todos os sites.</p>
          </div>
          <div class="col-lg-4 col-md-8 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
            <h4 class="title"><a href="">Segurança da Informação / Anti Virus</a></h4>
            <p class="description">Parcerias como Eset NOD, Kaspersky e BitDefender para manter a segurança da sua empresa sempre a frente! Planos Anuais.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
            <h4 class="title"><a href="">Hospedagem</a></h4>
            <p class="description">Hospedagem de sites, disponibilidade de serviço 99,9%! Proteção DDoS, alta velocidade, SEO para seu Dominio, Painel Cpanel com total administração da contratação! </p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-paper-outline"></i></div>
            <h4 class="title"><a href="">Licenciamento de S.O Windows</a></h4>
            <p class="description">Parceria Microsoft! Precisa de licenciamento de Windows, fale conosco!</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
            <h4 class="title"><a href="">Licenciamento de Pacote Office</a></h4>
            <p class="description">Office 365? Office Profissional? Temos as soluções que se encaixam na sua empresa e no seu bolso.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="icon ion-ios-cloud-done"></i></div>
            <h4 class="title"><a href="">Cloud Empresarial</a></h4>
            <p class="description">Ferramenta cloud para armazenamento de dados e compartilhamento de informações com empresas e colaboradores, backup dos dados em nuvem.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-people-outline"></i></div>
            <h4 class="title"><a href="">Desenvovimento de Marca / logo</a></h4>
            <p class="description">Sua identidade visual na net! Desenvolvimento de logos para sua empresa, banners e muito mais, pacotes completos para deixar a sua empresa com a sua cara. </p>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeIn">
      <div class="container text-center">
        <h3>Porque trazer sua empresa para o <span>Grupo Visentin</span>!</h3>
              
        <p><i>Nossa empresa cria parcerias e cultiva estratégias para contribuir com nosso compromisso de oferecer sempre serviços e produtos com alta qualidade, tecnologia de ultima geração e que valorizam o potencial dos nossos clientes.<i> </p>
        <a class="cta-btn btn-get-started scrollto" href="#contact">Minha Empresa na Internet</a>
      </div>
    </section><!-- #call-to-action -->

    <!--==========================
      Skills Section
    ============================-->
    <section id="skills">
      <div class="container">

        <header class="section-header">
          <h3>Nossas Habilidades</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
        </header>

        <div class="skills-content">

          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">HTML <i class="val">100%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">CSS <i class="val">90%</i></span>
            </div>
          </div>
          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Bootstrap <i class="val">99%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">JavaScript <i class="val">75%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Photoshop <i class="val">68%</i></span>
            </div>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">React Native <i class="val">88%</i></span>
            </div>
          </div>

        </div>

      </div>
    </section>

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Clientes e Parceiros</h3>
        </header>

        <div class="owl-carousel clients-carousel">
          <img src="img/clients/iso.png"  alt="">
          <img src="img/clients/microsoft.png" alt="">
          <img src="img/clients/client-4.png" alt="">
          <img src="img/clients/client-5.png" alt="">
          <img src="img/clients/client-6.png" alt="">
          <img src="img/clients/client-7.png" alt="">
          <img src="img/clients/client-8.png" alt="">
        </div>

      </div>
    </section><!-- #clients -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="testimonials" class="section-bg wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Clientes Satisfeitos</h3>
        </header>

        <div class="owl-carousel testimonials-carousel">

          <div class="testimonial-item">
            <img src="img/testimonial-1.jpg" class="testimonial-img" alt="">
            <h3>Elisangela Andres</h3>
            <h4>Route bike</h4>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
            <img src="img/testimonial-2.jpg" class="testimonial-img" alt="">
            <h3>Fernando Eleuterio</h3>
            <h4>Poli Sabores Tropicais</h4>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
            <img src="img/testimonial-3.jpg" class="testimonial-img" alt="">
            <h3>Joice Candido</h3>
            <h4>Joice Confecções</h4>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
            <img src="img/testimonial-4.jpg" class="testimonial-img" alt="">
            <h3>Matt Brandon</h3>
            <h4>Freelancer</h4>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

          <div class="testimonial-item">
            <img src="img/testimonial-5.jpg" class="testimonial-img" alt="">
            <h3>John Larson</h3>
            <h4>Entrepreneur</h4>
            <p>
              <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
              Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
              <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
            </p>
          </div>

        </div>

      </div>
    </section><!-- #testimonials -->

    <!--==========================
      Team Section
    ============================-->
    <section id="team">
      <div class="container">
        <div class="section-header wow fadeInUp">
          <h3>Time</h3>
          <p>Nossa equipe possui profissionais com certificações e conhecimento de sobra para criar e produzir o seu negócio na internet</p>
        </div>

        <div class="row">

          <div class="col-lg-4 col-md-6 wow fadeInUp">
            <div class="member">
              <img src="img/cido.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Aparecido Visentin</h4>
                  <span>CEO | Analista T.I</span>
                  <div class="social">
                    <a href="https://www.facebook.com/aparecidovisentin" target="blank_"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/cidin_luiz" target="blank_"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.linkedin.com/in/aparecido-luiz/" target="blank_"><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="member">
              <img src="img/altemar.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Altemar Visentin</h4>
                  <span>CEO | Financeiro</span>
                  <div class="social">
                   
                    <a href="https://www.facebook.com/altemar.peres" target="blank_"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/altemarperes/" target="blank_"><i class="fa fa-instagram"></i></a>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="member">
              <img src="img/joao.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>João paulo</h4>
                  <span>CEO | Web Developer</span>
                  <div class="social">
                    
                    <a href="https://www.facebook.com/joaopaulo.inacio.33"  target="blank_"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/jp_inacio97/" target="blank_"><i class="fa fa-instagram"></i></a>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #team -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

        <div class="section-header">
          <h3>Nossos Contatos</h3>
          <p>Nossa EQUIPE esta preparada para receber suas mensagens com exelência em atendimento ao CLIENTE</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Localização</h3>
              <address><a href="https://goo.gl/maps/Hr74NjVxYUG2" target="blank_">Rua Carlindo Abilio de Britto Nº 638 - Marialva, Parana-Brasil<a/></address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Telefone</h3>
              <p><a href="https://wa.me/+5544997509080?text=Olá Gostaria de mais informações!">+55 44 99750-9080</a></p>
              <p><a href="https://wa.me/+5544998911414?text=Olá Gostaria de mais informações!">+55 44 99891-1414</a></p>
              <p><a href="https://wa.me/+5544998848393?text=Olá Gostaria de mais informações!">+55 44 99884-8393</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:altemar@grupovisentin.com.br" align="">altemar@grupovisentin.com.br</a></p>
              <p><a href="mailto:joao@grupovisentin.com.br">joao@grupovisentin.com.br</a></p>
              <p><a href="mailto:aparecido@grupovisentin.com.br">aparecido@grupovisentin.com.br</a></p>
            </div>
          </div>

        </div>

        <div class="form">
          <div id="sendmessage">Entre em Contato conosco! :)</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Nome" data-rule="minlen:4" data-msg="Preencha seu nome!" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" data-rule="email" data-msg="Entre com um e-mail valido!" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Titulo" data-rule="minlen:4" data-msg="Titulo invalido" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Por favor ensira uma mensagem!" placeholder="Mensagem"></textarea>
              <div class="validation"></div>
            </div>
            <div class="g-recaptcha text-center"  data-sitekey="6Lch63wUAAAAAAcbAv3XAcKYdL6yxtJbYvJ9fv-l"></div>
            <div class="text-center"><button type="submit">Enviar Mensagem</button></div>
          </form>
        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>GVE</h3>
            <p>Empresa com seguemento de tecnologia vem visando no mercado nacional e internacional as melhores soluções e qualificações que impulsionam as pequenas, médias e grandes empresas a alcançar o sucesso.</p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Links</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="#" class="btn-get-started scrollto">Home</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#about" class="btn-get-started scrollto">Sobre Nós</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#services" class="btn-get-started scrollto">Serviços</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Termos de Serviço</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Politica de Privacidade</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contato</h4>
            <p>
              Rua Carlindo Abilio de Britto Nº 638 <br>
              Marialva - Paraná<br>
              Brasil <br>
              <strong>Fone:</strong><a href="https://wa.me/+5544998848393?text=Olá Gostaria de mais informações!">+55 44 99884-8393</a><br>
              <strong>Email:</strong><a href="mailto:contato@grupovisentin.com.br"> contato@grupovisentin.com.br</a><br>
            </p>

            <div class="social-links">
              <a href="skype:aparecido@grupovisenin.com.br" target="blank_" class="skype"><i class="fa fa-skype"></i></a>
              <a href="https://www.facebook.com/grupovisentin" target="blank_" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/grupovisentin/" target="blank_" class="instagram"><i class="fa fa-instagram"></i></a>
              
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Newsletter</h4>
            <p>Cadastre seu e-mail e receba novidades das nossas promoções e atualizações.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Inscrever" >
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Grupo Visentin Empresarial</strong>. Todos Direitos Reservados CNPJ000.000.000/0001-00
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!--captcha-->
  <!--script src='https://www.google.com/recaptcha/api.js?render=6Lch63wUAAAAAAcbAv3XAcKYdL6yxtJbYvJ9fv-l'></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
